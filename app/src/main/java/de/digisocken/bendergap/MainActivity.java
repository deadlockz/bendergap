package de.digisocken.bendergap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Process;
import android.provider.Settings;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class MainActivity extends Activity {
    private static final int Y_LIMIT = 100;
    private static final int X_LIMIT = 200;
    boolean fulls = false;
    private Canvas canvas;
    private View mainBack;

    int channelConfiguration = AudioFormat.CHANNEL_IN_MONO;
    int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
    int frequency = 8000;
    int analyseSize = 128;
    int bufferSize;
    AudioRecord audioRecord;
    double[] toTransform;
    RecordAudio recordTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        toFullscreen();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
        }

        mainBack = findViewById(R.id.mainBack);

        Bitmap.Config conf = Bitmap.Config.RGB_565;
        Bitmap bm = Bitmap.createBitmap(X_LIMIT, Y_LIMIT, conf);
        Drawable drw = new BitmapDrawable(getResources(), bm);
        mainBack.setBackground(drw);
        canvas = new Canvas(bm);

        bufferSize = AudioRecord.getMinBufferSize(
                frequency,
                channelConfiguration,
                audioEncoding
        );

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (bufferSize < (512 * 1024)) bufferSize = 512 * 1024;
        }

        audioRecord = new AudioRecord(
                MediaRecorder.AudioSource.MIC,
                frequency,
                channelConfiguration,
                audioEncoding,
                bufferSize
        );

        if(audioRecord.getState() != AudioRecord.STATE_INITIALIZED) {
            Toast.makeText(this, "Audio: INITIALIZATION ERROR", Toast.LENGTH_LONG).show();
            audioRecord.release();
            audioRecord = null;
        }

        toTransform = new double[analyseSize];
        recordTask = new RecordAudio();
        recordTask.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        toFullscreen();
    }

    @Override
    protected void onPause() {
        fulls = false;
        super.onPause();
    }

    public void toFullscreen() {
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (!fulls) {
            int uiOptions = getWindow().getDecorView().getSystemUiVisibility();
            int newUiOptions = uiOptions;


            if (Build.VERSION.SDK_INT >= 16) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
            }

            if (Build.VERSION.SDK_INT >= 18) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            }

            getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
            fulls = true;
        }
    }

    private class RecordAudio extends AsyncTask<Void, double[], Void> {
        String text = "";

        @Override
        protected Void doInBackground(Void... params) {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_MORE_FAVORABLE);
            //android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_LESS_FAVORABLE);
            try {
                short[] buffer = new short[analyseSize];
                double[] bufferParameter = new double[1];
                audioRecord.startRecording();
                while (true) {
                    int bufferReadResult = audioRecord.read(buffer, 0, analyseSize);

                    for (int i = 0; i < bufferReadResult; i++) {
                        toTransform[i] = (double) buffer[i] / 32768.0; // signed 16 bit
                    }
                    bufferParameter[0] = bufferReadResult; // :-(
                    publishProgress(toTransform, bufferParameter);
                }
                //audioRecord.stop();
            } catch (Throwable t) {
                //Log.e("AudioRecord", "Recording Failed");
            }
            return null;
        }

        protected void onProgressUpdate(double[]... toTransfoo) {
            int x,y;
            int lastY1 = 0;
            int lastX1 = 0;

            int lastY2 = 0;
            int lastX2 = 0;

            Paint p = new Paint();
            p.setColor(getResources().getColor(R.color.colorPrimary));
            canvas.drawRect(0, 0, X_LIMIT, Y_LIMIT, p);

            int color = getResources().getColor(R.color.colorAccent);
            p.setColor(color);

            for (x = 0; x < toTransfoo[1][0]; x++) {
                y = (int) (toTransfoo[0][x] * 500.0);
                //if (y>40) y = 40;
                //if (y<40) y =-40;

                if (y>=0) {
                    canvas.drawLine(lastX1*2, lastY1+57, x*2, y+57, p);
                    lastY1 = y;
                    lastX1 = x;
                }
                if (y<=0) {
                    canvas.drawLine(lastX2*2, lastY2+43, x*2, y+43, p);
                    lastY2 = y;
                    lastX2 = x;
                }
            }

            for(x=35; x < X_LIMIT; x+=42) {
                canvas.drawLine(x, 0, x, Y_LIMIT-1, p);
            }

            mainBack.invalidate();
        }
    }

}
