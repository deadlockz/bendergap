# Bender Gap

![logo](app/src/main/res/mipmap-xxxhdpi/ic_launcher.png)

This android app records your voice and converts your mobile into a Bender (Futurama) mouth.

From Idea to APK in just 2.5 hours - do not mess me up with issues or merge requests.

# Download

Get APK (4.1+) from [here](https://gitlab.com/deadlockz/bendergap/raw/master/app/release/de.digisocken.bendergap.apk?inline=false)
